import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { StringGeneratorService } from 'src/app/services/string-generator-service.service';
import {map} from 'rxjs/operators'

@Component({
  selector: 'app-string-generator',
  templateUrl: './string-generator.component.html',
  styleUrls: ['./string-generator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StringGeneratorComponent implements OnInit, OnDestroy {

  generatorInterval: Observable<number> = interval(3000)
  randomString: string = ''

  subscriptions$: Subscription[] = []

  constructor(
    private stringGeneratorService: StringGeneratorService,
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.subscriptions$.push(
        this.generatorInterval
        .pipe(map(() => this.stringGeneratorService.generateRandomString(5)))
        .subscribe((str) => {
          this.randomString = str
          this.cdr.detectChanges()
      })
    )
  }

  get isPalindrome(): boolean {
    return this.stringGeneratorService.checkIsPalindrome(this.randomString)
  }
  get hasZero(): boolean {
    return this.stringGeneratorService.checkIsZero(this.randomString)
  }
  get hasAllDigits(): boolean {
    return this.stringGeneratorService.checkIsDigits(this.randomString)
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(sub => sub.unsubscribe())
  }

}
