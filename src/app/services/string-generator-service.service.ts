import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class StringGeneratorService {

  characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  constructor() {

  }

  generateRandomString(length: number): string {
    let res = '';
    for (let i = 0; i < length; i++) {
      res += this.characters.charAt(Math.floor(Math.random() * this.characters.length))
    }

    return res
  }

  checkIsPalindrome(str: string): boolean {
    const reversedStr = str.split('').reverse().join('')
    return reversedStr === str
  }

  checkIsDigits(str: string): boolean {
    return /^\d+$/.test(str)
  }

  checkIsZero(str: string): boolean {
    return str.includes('0')
  }


}
